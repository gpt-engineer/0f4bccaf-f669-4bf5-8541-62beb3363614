document.addEventListener("DOMContentLoaded", function () {
  const form = document.getElementById("waitlistForm");

  form.addEventListener("submit", async function (e) {
    e.preventDefault(); // Prevent the default form submission

    const email = form.querySelector('input[type="email"]').value;
    const productDescription = form.querySelector("textarea").value;

    // Construct the POST request
    const response = await fetch(
      "https://lovable-waitlist.fabian-hedin.workers.dev",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          product_description: productDescription,
        }),
      }
    );

    // Handle the response
    const result = await response.json();

    if (result.success) {
      // Display the thank you message
      form.innerHTML =
        '<p class="text-2xl mt-6">Thank you for joining the waitlist!</p>';
    } else {
      // Handle any errors (Optional)
      form.innerHTML = `<p class="text-red-600 text-2xl mt-6">Error: ${result.message}</p>`;
    }
  });
});
